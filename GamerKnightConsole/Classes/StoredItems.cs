﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineConsole.Classes
{
    public class StoredInventory : Item
    {
        #region Fields
        private int _count;
        private RandomItemGenerator _box;
        private Item[] _items;
        private string _name;
        #endregion


        #region Properties
        public Item Box { get { return _box; }  }
        public Item[] Items { get { return _items; } }
        public int Count { get { return _count; } }
        public int Capacity { get; }
        public new string Name { get { return _name; } }
        #endregion


        #region Constructors
        public StoredInventory(int size)
        {
            this._name = "Bag ";
            _items = new Item[size];
            _count = 0;
        }

        #endregion


        #region Methods      
        public Item GetItem(int num)
        {
            return this._items[num];
        }

        public Item SetItem(int index, Item item)
        {
            item = _items[index];
            return item;
        }


        public void AddItem(Item item)
        {
            _items[_count] = item;
            _count++;
        }

        public void RemoveItem(Item item, int index)
        {
            _items[index] = null;
            _count--;
        }

        //calcualte total Inventory Weight
        public double calcWeight()
        {
            double totalWeight = 0;
            for (int i = 0; i < _items.Length; i++)
            {
                totalWeight += _items[i].Weight;
            }
            return totalWeight;
        }

        //Stored Item Indexer
        public Item this[int index]
        {
            get
            {
                return _items[index];
            }
        }

        public override string ToString()
        {
            string x = "";
            for (int i = 0; i < _items.Length; i++)
            {
                if (_items[i] == null)
                {
                    x += "Empty\t";
                }
                else
                {
                    x += _items[i] + "\t";
                }
            }
            return x;
        }
        #endregion
    }
}

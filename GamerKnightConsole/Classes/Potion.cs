﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineConsole.Classes
{
    public class Potion : Item, IDefensePower
    {
        private int _healthBonus; private string[] _names = new string[4]
        {
            "Small Potion","Medium Potion","Large Potion","Super Potion"
        };
        private string _name;
        private InventorySlotId _slot;

        public int HealthBonus { get { return _healthBonus; } }
        public string Name { get { return _name; } }
        private InventorySlotId Slot { get { return _slot; } }

        public Potion(int name)
        {
            this._slot = InventorySlotId.POTION1;
            this._name = _names[name];
            this.getDefense(name);
        }

        public void getDefense(int num = 666)
        {
            int ran;
            if (num != 666)
            {
                ran = rand.Next(0, 20);
                _healthBonus = 2 + ran;
            }
            else
            {
                if (num == 0)
                {
                    ran = rand.Next(0, 3);
                    _healthBonus = 0 + ran;
                }
                if (num == 1)
                {
                    ran = rand.Next(0, 3);
                    _healthBonus = 2 + ran;
                }
                if (num == 2)
                {
                    ran = rand.Next(0, 3);
                    _healthBonus = 4 + ran;
                }
                if (num == 3)
                {
                    ran = rand.Next(0, 3);
                    _healthBonus = 3 + ran;
                }
            }
        }

        public override string ToString()
        {
            return this.Name + "\tDefense: " + this.HealthBonus + "\tSlot: " + this.Slot;
        }
    }
}

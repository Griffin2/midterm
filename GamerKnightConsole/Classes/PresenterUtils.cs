﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineConsole.Classes
{
    public class PresenterUtils
    { 
        /// <summary>
        /// INCORRECT IMPLEMENTATIONS
        /// 
        /// Use To Display What?
        /// 
        /// 11/09/17
        /// </summary>


        public static string GetSlotName(InventorySlotId slot)
        {
            return slot.ToString();
        }
        public static string Display(InventorySlotId slot)
        {
            return slot.ToString();
        }
        public static string Display(Item item)
        {
            return item.ToString();
        }
        public static string Display(Item[] item)
        {
            return item.ToString();
        }
        public static string Display(StoredInventory items)
        {
            return items.ToString();
        }
        public static string Display(Active items)
        {
            return items.ToString();
        }
        public static string Display(Character person, bool ShowBagContents)
        { 
            ShowBagContents = true;
            return person.ToString();
        }
    }
}

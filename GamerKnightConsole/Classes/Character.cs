﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineConsole.Classes
{
    public class Character : Active
    {
        #region Fields
        //stored items
        protected StoredInventory _bag;
        //equipped items
        protected Active _equipped;
        protected string _name;
        protected int _currentHealth;
        private bool _dead;
        private int _cClass;
        private int _amount = 0;
        #endregion


        #region Properties
        public StoredInventory Bag { get { return _bag; }  }
        public int CClass { get { return _cClass; } }
        public string Name { get; set; }
        public int CurrentHealth
        {
            get
            {
                if (_currentHealth < 1)
                {
                    return 0;
                }
                else if (_currentHealth > 100)
                {
                    return 100;
                }
                else
                {
                    return _currentHealth;
                }
            }
        }
        public bool IsDead
        {
            get
            {
                if (CurrentHealth == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public int Amount { get { return _amount; } }
        #endregion


        #region Constructors
        public Character( )
        {
            this._cClass = 0;
            this._currentHealth = 100;
            this._dead = false;
        }
        public Character( int health,  bool dead) 
        {
            this._currentHealth = health;
            this._dead = dead;
        }
        #endregion



        #region Methods
        public void SetClass(int num)
        {
            _cClass = num;
        }

        public void SetAttack(int num)
        {
            _amount = num;
        }
        public void SetDefense(int num)
        {
            _currentHealth += num;
        }

        public void SetBag(StoredInventory bag)
        {
            _bag = bag;
        }

        public int CalcTotalAttackValue(int num = 0)
        {
            //x is class boost
            //num can be potion or weapon boost
            int x = 0;
            //Enemies Rat
            if (_cClass == 10)
            {
                x =  3;
            }
            //Enemy Dog
            else if (_cClass == 11)
            {
                x = 6;
            }
            //Enemy BatS
            else if (_cClass == 12)
            {
                x = 3;
            }
            //Player Goblin
            else if (_cClass == 1)
            {
                x = 6;
            }
            //Dwarf
            else if(_cClass == 2)
            {
                x = 3;
            }
            //Ogre
            else if(_cClass == 3)
            {
                x = 10;
            }
            //Human
            else if(_cClass == 4)
            {
                x = 5;
            }
            //PURPOSE: debugger
            else
            {
                int ran;
                ran = rand.Next(0, 9);
                x = 24 + ran;
            }

            x += num + _amount;


            return x;
        }

        public int CalcTotalDefense(int num = 0)
        {
            return _currentHealth += num;
        }

        public void TakeDamage(int damage = 0)
        {
            this._currentHealth -= damage;
            if (this._currentHealth >= 1)
            {
                _dead = false;
            }
            else
            {
                _dead = true;
            }
        }

        public void Pickup(Item item)
        {
            this._equipped = (Active)item;
        }

        public void UnequippedAll()
        {

        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineConsole.Classes
{
    public class Active : Item
    {
        #region Fields
        private Item[] _slots;
        private InventorySlotId _slot;
        #endregion


        #region Properties
        public Item[] Slots { get{return _slots;} }
        public InventorySlotId Slot { get; }
        #endregion


        #region Constructors
        public Active()
        {
            _slots = new Item[7];
        }
        #endregion


        #region Methods
        public Item GetItem(InventorySlotId x)
        {
            this._slot = x;
            for (int i = 0; i < _slots.Length; i++)
            {
                if (_slots[i] != null)
                {
                    if (x == _slot)
                    {
                        return this._slots[i];
                    }
                }
            }
            return null;
        }

        public Item Equip(InventorySlotId slot, Item item)
        {
            Item newItem = item;

            for (int i = 0; i < _slots.Length; i++)
            {
                switch (slot)
                {
                    case InventorySlotId.UNEQUIPPABLE:
                        _slots[i] = item;
                        break;
                    case InventorySlotId.HELMET:
                        _slots[i] = item;
                        break;
                    case InventorySlotId.CHESTPIECE:
                        _slots[i] = item;
                        break;
                    case InventorySlotId.GRIEVES:
                        _slots[i] = item;
                        break;
                    case InventorySlotId.VAMBRACES:
                        _slots[i] = item;
                        break;
                    case InventorySlotId.WEAPON:
                        _slots[i] = item;
                        break;
                    case InventorySlotId.POTION1:
                        _slots[i] = item;
                        break;
                    case InventorySlotId.POTION2:
                        _slots[i] = item;
                        break;
                    default:
                        break;
                }
                if (_slots[i].Slot == slot)
                {
                    newItem = _slots[i];
                    return newItem;
                }
            }
            return newItem;
        }

        public Item UnEquip(InventorySlotId slot)
        {
            Item nullItem = null;

            for (int i = 0; i < _slots.Length; i++)
            {
                switch (slot)
                {
                    case InventorySlotId.UNEQUIPPABLE:
                        _slots[i] = null;
                        break;
                    case InventorySlotId.HELMET:
                        _slots[i] = null;
                        break;
                    case InventorySlotId.CHESTPIECE:
                        _slots[i] = null;
                        break;
                    case InventorySlotId.GRIEVES:
                        _slots[i] = null;
                        break;
                    case InventorySlotId.VAMBRACES:
                        _slots[i] = null;
                        break;
                    case InventorySlotId.WEAPON:
                        _slots[i] = null;
                        break;
                    case InventorySlotId.POTION1:
                        _slots[i] = null;
                        break;
                    case InventorySlotId.POTION2:
                        _slots[i] = null;
                        break;
                    default:
                        break;
                }
                if (_slots[i].Slot == slot)
                {
                    nullItem = _slots[i];
                    return nullItem;
                }
            }
            return nullItem;
        }

        public int CalcTotalWeight()
        {
            //return the value of total weight in active bag
            double weight = 0;
            for (int i = 0; i < _slots.Length; i++)
            {
                weight += _slots[i].Weight;
            }
            return Convert.ToInt32(weight);
        }

        public int CalcTotalAttackValue()
        {
            for (int i = 0; i < _slots.Length; i++)
            {
                if (_slots[i] != null)
                {
                    return 2;
                }
            }
            return 0;
        }

        public int CalcTotalDefenseValue()
        {
            int temp = 0;
            for (int i = 0; i < _slots.Length; i++)
            {
            }
            return 0;
        }

        public Item this[int index]
        {
            get
            {
                return _slots[index];
            }
           
        }

        #endregion

    }
}

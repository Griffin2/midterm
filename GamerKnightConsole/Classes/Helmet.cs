﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineConsole.Classes
{
    public class Helmet : Item, IDefensePower
    {
        private int _defensePower;
        private InventorySlotId _slot;
        private string _name;
        private string[] _names = new string[4]
        {
            "Bronze Helmet","Iron Helmet","Steel Helmet","Spiky Helmet"
        };

        public int DefensePower { get { return _defensePower; } }
        public InventorySlotId Slot { get { return _slot; } }
        public new string Name { get{return _name; } }

        public Helmet()
        {
            this._name = "BASIC HELMET";
            this.getDefense(0);
            this._slot = InventorySlotId.HELMET;
        }

        public Helmet(int name)
        {
            this._name = _names[name];
            this.getDefense(name);
            this._slot = InventorySlotId.HELMET;
        }

        public Helmet(string name, int power)
        {
            this._name = name;
            this._defensePower = power;
        }

        public void getDefense(int num = 666)
        {
            int ran;
            if (num != 666)
            {
                ran = rand.Next(0, 20);
                _defensePower = 2 + ran;
            }
            else
            {
                if (num == 0)
                {
                    ran = rand.Next(0, 3);
                    _defensePower = 0 + ran;
                }
                if (num == 1)
                {
                    ran = rand.Next(0, 3);
                    _defensePower = 2 + ran;
                }
                if (num == 2)
                {
                    ran = rand.Next(0, 3);
                    _defensePower = 4 + ran;
                }
                if (num == 3)
                {
                    ran = rand.Next(0, 3);
                    _defensePower = 3 + ran;
                }
            }
        }

        public override string ToString()
        {
            return this.Name + "\tDefense: " + this.DefensePower + "\tSlot: " + this.Slot;
        }
    }
}

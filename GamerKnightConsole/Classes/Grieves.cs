﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineConsole.Classes
{
    public class Grieves : Item
    {
        private string[] _names = new string[3]
         {
            "Bronze Grieves", "Iron Grieves","Steel Grieves"
         };
        private string _name;
        private int _healthBonus;
        private InventorySlotId _slot;

        public InventorySlotId Slot { get { return _slot; } }
        public string Name { get { return _name; } }
        public int HealthBonus { get { return _healthBonus; } }

        public Grieves(int name)
        {
            this._slot = InventorySlotId.GRIEVES;
            this._name = _names[name];
            this.getDefense(name);
        }

        public void getDefense(int num = 666)
        {
            int ran;
            if (num != 666)
            {
                ran = rand.Next(0, 2);
                _healthBonus = 2 + ran;
            }
            else
            {
                if (num == 0)
                {
                    ran = rand.Next(0, 2);
                    _healthBonus = 1 + ran;
                }
                if (num == 1)
                {
                    ran = rand.Next(0, 3);
                    _healthBonus = 2 + ran;
                }
                if (num == 2)
                {
                    ran = rand.Next(0, 3);
                    _healthBonus = 4 + ran;
                }
            }
        }
        public override string ToString()
        {
            return this.Name + "\tDefense: " + this.HealthBonus + "\tSlot: " + this.Slot;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineConsole.Classes
{
    public class GameManager: Character
    {
        #region Fields
        private Character _player;
        private Character _enemy;
        private bool _matchOver;
        private int _depth = 0;
        #endregion

        #region Properties
        public Character Player { get { return _player; }  }
        public Character Enemy { get { return _enemy; }  }
        public int Depth { get { return _depth; } }
        public bool MatchOver { get { return ShowGameOver(); }  }
        #endregion

        #region Constructor
        public GameManager(bool tmo, int level, Character player = null) 
        {
            this._player = player;
            this._enemy = new Character();
            this._matchOver = tmo;
            this._depth = level;
        }
        #endregion

        #region Methods
        //if the player dies
        private bool ShowGameOver()
        {
            bool temp = false;
            if (this.Player.IsDead)
            {
                temp = true;
                this._player.Name = "PLAYER DIED";
            }
            return temp;
        }

        public void SetCharacter(Character person)
        {
            this._player = person;
        }

        public void SetEnemy(Character person)
        {
            this._matchOver = false;
            this._enemy = person;
        }

        public void SetLevel(int num)
        {
            this._depth = num;
        }

        public void Attack(Character attacker, Character defender)
        {
            int num = rand.Next(0, 11);
            if (num < 8)
            {
                //attack value - defender health
                defender.TakeDamage(attacker.CalcTotalAttackValue());
            }
            if (num == 5)
            {
                //critical hit 
                //attack value(+5) - defender health
                defender.TakeDamage(attacker.CalcTotalAttackValue(num));
            }
            //ELSE MISSED HIT
        }

        public void ManageInventory()
        {
            if (this.MatchOver == true)
            {
                //allow to change items in stored and active arrays
            }
        }
        
        //if player is alive create new enemy
        public void NextBattle()
        {
            this._depth++;
            this._matchOver = true;
            //generate random enemy and set mathc over to false after a set time
        }

        public void LeaveArena()
        {
            this._matchOver = true;
            this._depth = 0;
        }
        #endregion
    }
}
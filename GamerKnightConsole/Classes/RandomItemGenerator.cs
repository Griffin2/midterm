﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EngineConsole.Classes
{
    public class RandomItemGenerator : Item
    {
        private Random _random = new Random();
        
        public int AttackValue { get { return getAttackValue(); } }
        public int DefenseValue { get { return getDefenseValue(); } }
        public int TotalValue { get { return getAttackValue() + getDefenseValue(); } }


        public string  Name { get; set; }

        public RandomItemGenerator()
        {
            this.Name = "Generator";
        }

        public Item GenerateNewItem(int num = 666)
        {
            int ran;
            if (num == 666)
            {
                num += rand.Next(0, 5);
            }

            Item Power;

            if (num == 0)
            {
                Power = new Helmet(rand.Next(0, 3));
            }
            else if (num == 1)
            {
                Power = new Weapon(rand.Next(0, 3));
            }
            else if (num == 2)
            {
                Power = new ChestPiece(rand.Next(0, 2));
            }
            else if (num == 3)
            {
                Power = new Grieves(rand.Next(0, 2));
            }
            else if (num == 4)
            {
                Power = new Vambraces(rand.Next(0, 2));
            }
            else
            {
                Power = new Potion(rand.Next(0, 3));
            }
            return Power;
        }

        private int getDefenseValue()
        {
            int x = _random.Next(1, 9);

            int d = x;

            return d;
        }

        public int getAttackValue()
        {
            int x = _random.Next(1, 9);

            int attack = x;

            return attack;
        }

    }
}

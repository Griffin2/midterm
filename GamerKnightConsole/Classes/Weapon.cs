﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineConsole.Classes
{
    public class Weapon : Item, IAttackPower
    {
        private int _attackBonus;
        private string _name;
        private InventorySlotId _slot;
        private string[] _names = new string[4]
        {
            "Bronze Sword","Iron Sword","Steel Sword","Buckler"
        };

        public int AttackBonus { get { return _attackBonus; } }
        public InventorySlotId Slot { get { return _slot; } }
        public string Name { get { return _name; } }

        public Weapon(int name = 0)
        {
            this._name = _names[name];
            this.getAttackValue(name);
            this._slot = InventorySlotId.WEAPON;
        }

        public void getAttackValue(int num = 666)
        {
            int ran;
            if (num != 666)
            {
                ran = rand.Next(0, 20);
                _attackBonus = 2 + ran;
            }
            else
            {
               if(num == 0)
               {
                    ran = rand.Next(0, 6);
                    _attackBonus = 4 + ran;
               }
               if (num == 1)
               {
                    ran = rand.Next(0, 6);
                    _attackBonus = 12 + ran;
                }
               if (num == 2)
               {
                    ran = rand.Next(0, 6);
                    _attackBonus = 12 + ran;
                }
               if (num == 3)
               {
                    ran = rand.Next(0, 6);
                    _attackBonus = 4 + ran;
                }
            }
        }
        public override string ToString()
        {
            return this.Name + "\tAttack: " + this.AttackBonus + "\tSlot: " + this.Slot;
        }
    }
}

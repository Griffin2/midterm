﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngineConsole.Classes
{
    public abstract class Item : IComparable
    {
        #region Fields
        public Random rand = new Random();

        private Guid _id;

        private string _name;
        private double _weight;
        InventorySlotId _slot;
        #endregion


        #region Properties
        public Guid Id { get; }
        public string Name { get { return _name; } set { _name = value; }   }
        public double Weight { get { return _weight; }  }
        public InventorySlotId Slot { get { return _slot; }  }
        #endregion


        #region Constructors
        //cannot create concrete objects with abstract methods
        #endregion


        #region Methods
        //compare wieght
        public int CompareTo(object obj)
        {
            Item other = (Item)obj;
            return this.CompareTo(obj);
        }

        public override bool Equals(object obj)
        {
            Item other = (Item)obj;
            //if Names are even return true
            return (this.Name == other.Name ? true : false);
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
        public override string ToString()
        {
            return base.ToString();
        }
        #endregion
    }


}

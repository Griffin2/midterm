﻿using EngineConsole.Classes;
using System;
using System.Media;
using System.Threading;

namespace GamerKnightConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            SoundPlayer simpleSound = new SoundPlayer(@"C:\Users\Boss\Desktop\KnightKnight\Sounds\BeepBox.wav");
            simpleSound.Play();
            Console.ForegroundColor = ConsoleColor.DarkGray;

            /////////////////////////////////////////////////////
            //11/14/17
            //
            //BAG IS JANK
            //
            //NEXT level doesn't add to attack
            //
            //LOOTING SYSTEM
            //////////////////////////////////////////////////
            RandomItemGenerator box = new RandomItemGenerator();
            Random ranNum = new Random();

            //GAME MANAGERS
            GameManager Play = new GameManager(false, 0);

            //ITEMS//////////////////////////////////////////////////
            //HELMETS
                Helmet[] hats = new Helmet[4];
                hats[0] = new Helmet(0);
                Thread.Sleep(80);
                hats[1] = new Helmet(1);
                Thread.Sleep(80);
                hats[2] = new Helmet(2);
                Thread.Sleep(80);
                hats[3] = new Helmet(3);

            //WEAPONS
                Weapon[] weapons = new Weapon[8];
                weapons[0] = new Weapon(0);
                Thread.Sleep(80);
                weapons[1] = new Weapon(1);
                Thread.Sleep(80);
                weapons[2] = new Weapon(2);
                Thread.Sleep(80);
                weapons[3] = new Weapon(3);

            //GRIEVES
                Grieves[] grief = new Grieves[8];
                grief[0] = new Grieves(0);
                Thread.Sleep(80);
                grief[1] = new Grieves(1);
                Thread.Sleep(80);
                grief[2] = new Grieves(2);

            //CHESTPIECES
                ChestPiece[] chest = new ChestPiece[8];
                chest[0] = new ChestPiece(0);
                Thread.Sleep(80);
                chest[1] = new ChestPiece(1);
                Thread.Sleep(80);
                chest[2] = new ChestPiece(2);

            //VAMBRACES
                Vambraces[] vambrace = new Vambraces[8];
                vambrace[0] = new Vambraces(0);
                vambrace[1] = new Vambraces(1);
                vambrace[2] = new Vambraces(2);

            //POTIONS
                Potion[] potions = new Potion[4];
                potions[0] = new Potion(0);
                potions[1] = new Potion(1);
                potions[2] = new Potion(2);
                potions[3] = new Potion(3);

            //STOREDITEMS
            StoredInventory Bag = new StoredInventory(20);

            //ACTIVE ITEMS
            Active Actives = new Active();

            //CHARACTERS
            Character player = new Character();
            Character[] enemies = new Character[4];
            Character[] boss = new Character[4];


            //INTS
            int milliseconds = 300;
            int last;
            int count = 0;
            int inputNum;
            int level = 0;
            int time = 0;
            int playAttack = 0;
            int playDefense = 0;
            int ran;
            int bossNum = 0;
            int counter = 0;

            //STRINGS
            string[] classes = new string[4] { "Goblin", "Dwarf", "Ogre", "Human" };
            string[] enemyNames = new string[4] { "Wild Rat", "Dog", "Goblin", "Kobolt" };
            string[] enemyBoss = new string[4] { "Monster", "Dinosaur", "Wizard", "Gorilla" };
            string[] ItemBagNames = new string[20];
            string[] ActiveBagNames = new string[7];
            string input;
            string response;


            //BOOLS
            bool correct = true;


            //BEGIN
            for (;;)
            {
                do
                {
                    Console.Clear();
                    Console.WriteLine("Start (y)  ");
                    input = Console.ReadLine();
                    if (input.ToLower() == "y" || input.ToLower() == "yes" || input.ToLower() == "yea")
                    {
                        Console.Clear();
                        count = 1;
                    }
                    else
                    {
                        count = 0;
                    }
                } while (count == 0);

                Console.Clear();
                Console.Title = "ASCII Art";
                string winLogo = @"
 _      _  _      _      _____ ____  _  _ 
/ \  /|/ \/ \  /|/ \  /|/  __//  __\/ \/ \
| |  ||| || |\ ||| |\ |||  \  |  \/|| || |
| |/\||| || | \||| | \|||  /_ |    /\_/\_/
\_/  \|\_/\_/  \|\_/  \|\____\\_/\_\(_)(_)
                                          
";
                string title = @"
                                                                                               
,--. ,--.,--.               ,--.       ,--.      ,--. ,--.        ,--.       ,--.       ,--.   
|  .'   /`--',--,--,  ,---. |  ,---. ,-'  '-.    |  .'   /,--,--, `--' ,---. |  ,---. ,-'  '-. 
|  .   ' ,--.|      \| .-. ||  .-.  |'-.  .-'    |  .   ' |      \,--.| .-. ||  .-.  |'-.  .-' 
|  |\   \|  ||  ||  |' '-' '|  | |  |  |  |      |  |\   \|  ||  ||  |' '-' '|  | |  |  |  |   
`--' '--'`--'`--''--'.`-  / `--' `--'  `--'      `--' '--'`--''--'`--'.`-  / `--' `--'  `--'   
                     `---'                                            `---'                    
";
                string credit = @"
  ________      .__  _____  _____  _____.__     /\             ________                       
 /  _____/______|__|/ ____\/ ____\/ ____\__| ___)/  ______    /  _____/_____    _____   ____  
/   \  __\_  __ \  \   __\\   __\\   __\|  |/    \ /  ___/   /   \  ___\__  \  /     \_/ __ \ 
\    \_\  \  | \/  ||  |   |  |   |  |  |  |   |  \\___ \    \    \_\  \/ __ \|  Y Y  \  ___/ 
 \______  /__|  |__||__|   |__|   |__|  |__|___|  /____  >    \______  (____  /__|_|  /\___  >
        \/                                      \/     \/            \/     \/      \/     \/ 
";
                for (int i = 0; i < 5; i++)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(credit);
                    Console.WriteLine(title);


                    Thread.Sleep(milliseconds);
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(credit);
                    Console.WriteLine(title);

                    Thread.Sleep(milliseconds);
                    Console.Clear();
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(credit);
                    Console.WriteLine(title);

                    Thread.Sleep(milliseconds);
                }
                Console.ForegroundColor = ConsoleColor.DarkGray;

                //BUILD CHARACTER
                input = "";
                response = "";
                if (input == "")
                {
                    for (; ; )
                    {
                        Console.Clear();
                        Thread.Sleep(320);
                        Console.WriteLine("What shall I call you?");
                        response = Console.ReadLine();

                        Console.WriteLine("{0} ..really? y/n", response);
                        input = Console.ReadLine();

                        if (input.ToLower() == "y" || input.ToLower() == "yes" || input.ToLower() == "yea")
                        {
                            break;
                        }
                        if (input.ToLower() == "n" || input.ToLower() == "no" || input.ToLower() == "nah")
                        {
                            input = "";
                        }
                        Console.ReadKey();
                    }
                    player.Name = response.ToLower();
                }


                count = 0;
                do
                {
                    Console.Clear();
                    Console.WriteLine("Select A Class {0}", player.Name);
                    Console.WriteLine("1  " + classes[0]);
                    Console.WriteLine("2  " + classes[1]);
                    Console.WriteLine("3  " + classes[2]);
                    Console.WriteLine("4  " + classes[3]);

                    while (!Int32.TryParse(Console.ReadLine(), out inputNum))
                    {
                        Console.Clear();
                        Console.WriteLine("Select A Class: ");
                        Console.WriteLine("1  " + classes[0]);
                        Console.WriteLine("2  " + classes[1]);
                        Console.WriteLine("3  " + classes[2]);
                        Console.WriteLine("4  " + classes[3]);
                        Console.WriteLine("  --try-again--");
                    }

                    switch (inputNum)
                    {
                        case 1:
                            Console.WriteLine("{0} Nice!", classes[0]);
                            player.SetClass(inputNum);
                            Console.WriteLine("Attack: {0}", player.CalcTotalAttackValue());
                            Console.WriteLine("Health: {0}", player.CurrentHealth);
                            Console.WriteLine("Satisfied? y/n");
                            input = Console.ReadLine();
                            if (input.ToLower() == "y" || input.ToLower() == "yes" || input.ToLower() == "yea")
                            {
                                Console.WriteLine("Lets Begin!");
                                count = 1;
                            }
                            break;
                        case 2:
                            Console.WriteLine("{0} Nice!", classes[1]);
                            player.SetClass(inputNum);
                            Console.WriteLine("Attack: {0}", player.CalcTotalAttackValue());
                            Console.WriteLine("Health: {0}", player.CurrentHealth);
                            Console.WriteLine("Satisfied? y/n");
                            input = Console.ReadLine();
                            if (input.ToLower() == "y" || input.ToLower() == "yes" || input.ToLower() == "yea")
                            {
                                Console.WriteLine("Lets Begin!");
                                count = 1;
                            }
                            break;
                        case 3:
                            Console.WriteLine("{0} Nice!", classes[2]);
                            player.SetClass(inputNum);
                            Console.WriteLine("Attack: {0}", player.CalcTotalAttackValue());
                            Console.WriteLine("Health: {0}", player.CurrentHealth);
                            Console.WriteLine("Satisfied? y/n");
                            input = Console.ReadLine();
                            if (input.ToLower() == "y" || input.ToLower() == "yes" || input.ToLower() == "yea")
                            {
                                Console.WriteLine("Lets Begin!");
                                count = 1;
                            }
                            break;
                        case 4:
                            Console.WriteLine("{0} Nice!", classes[3]);
                            player.SetClass(inputNum);
                            Console.WriteLine("Attack: {0}", player.CalcTotalAttackValue());
                            Console.WriteLine("Health: {0}", player.CurrentHealth);
                            Console.WriteLine("Satisfied? y/n");
                            input = Console.ReadLine();
                            if (input.ToLower() == "y" || input.ToLower() == "yes" || input.ToLower() == "yea")
                            {
                                Console.Clear();
                                Console.WriteLine("Lets Begin!");
                                count = 1;
                            }
                            break;
                    }
                } while (count == 0);

                Play.SetCharacter(player);
                Play.Player.SetBag(Bag);

                //DISPLAY THE BEGINING ITEMS
                do
                {
                    Console.Clear();
                    Console.WriteLine("If Item is in a Slot.. Item is Active");
                    Console.ReadLine();
                    Console.Clear();
                    for (int i = 0; i < ItemBagNames.Length; i++)
                    {
                        ItemBagNames[i] = "empty";
                    }
                    ItemBagNames[0] = chest[0].ToString();
                    ItemBagNames[1] = weapons[0].ToString();
                    ItemBagNames[2] = potions[0].ToString();
                    ActiveBagNames[0] = ItemBagNames[0];
                    ActiveBagNames[1] = ItemBagNames[1];
                    ActiveBagNames[2] = ItemBagNames[2];
                    ItemBagNames[0] = "empty";
                    ItemBagNames[1] = "empty";
                    ItemBagNames[2] = "empty";

                    Console.WriteLine("{0}'s Active Inventory", Play.Player.Name);
                    inputNum = 0;
                    foreach (var item in ActiveBagNames)
                    {
                        inputNum++;
                        Console.WriteLine(item);
                    }

                    playAttack += weapons[0].AttackBonus;
                    playDefense += chest[0].HealthBonus + potions[0].HealthBonus;

                    Console.ReadLine();
                    Console.WriteLine();
                    Console.WriteLine("Ready For Battle? y/n");
                    input = Console.ReadLine();
                    if (input.ToLower() == "y" || input.ToLower() == "yes" || input.ToLower() == "yea")
                    {
                        Console.WriteLine("Lets Begin!");
                        count = 1;
                    }
                    else
                    {
                        count = 0;
                    }
                } while (count == 0);


                //INSTANTIATE ENEMIES//
                for (int i = 0; i < enemies.Length; i++)
                {
                    enemies[i] = new Character();
                    enemies[i].Name = enemyNames[i];
                    enemies[i].SetClass(10);
                }

                //INSTANTIATE BOSS'//
                for (int i = 0; i < boss.Length; i++)
                {
                    boss[i] = new Character();
                    boss[i].Name = enemyBoss[i];
                    ran = ranNum.Next(0, 14);
                    boss[i].SetClass(ran);
                }


                //COMBAT
                correct = false;
                count = 0;
                time = 2;

                bossNum = 0;
                Play.Player.SetAttack(playAttack);
                Play.Player.SetDefense(playDefense);
                Play.SetEnemy(enemies[Play.Depth]);
                try
                {
                    do
                    {
                        //attempt to update attack
                        for (int i = 0; i <= time; i++)
                        {
                            if (ActiveBagNames[i] == potions[i].ToString() || ActiveBagNames[i] == hats[i].ToString() || ActiveBagNames[i] == chest[i].ToString() || ActiveBagNames[i] == vambrace[i].ToString() || ActiveBagNames[i] == grief[i].ToString() || ActiveBagNames[i] == weapons[i].ToString())
                            {
                                if (ActiveBagNames[i] == potions[i].ToString())
                                {
                                    Play.Player.SetDefense(potions[i].HealthBonus);
                                }
                                if (ActiveBagNames[i] == hats[i].ToString())
                                {
                                    Play.Player.SetAttack(hats[i].DefensePower);
                                }
                                if (ActiveBagNames[i] == chest[i].ToString())
                                {
                                    Play.Player.SetDefense(chest[i].HealthBonus);
                                }
                                if (ActiveBagNames[i] == vambrace[i].ToString())
                                {
                                    Play.Player.SetDefense(vambrace[i].HealthBonus);
                                }
                                if (ActiveBagNames[i] == grief[i].ToString())
                                {
                                    Play.Player.SetDefense(grief[i].HealthBonus);
                                }
                                if (ActiveBagNames[i] == weapons[i].ToString())
                                {
                                    Play.Player.SetDefense(weapons[i].AttackBonus);
                                }
                            }
                        }

                        if (Play.Player.IsDead)
                        {
                            count = 1;
                            break;
                        }

                        while (!Play.MatchOver)
                        {
                            Console.Clear();
                            Console.WriteLine("{0}\t Attack:{1}\tHealth:{2}\n", Play.Player.Name, Play.Player.CalcTotalAttackValue(), Play.Player.CurrentHealth);
                            Console.WriteLine("The {0} has a health of {1} and an attack of {2} Good Luck!", Play.Enemy.Name, Play.Enemy.CurrentHealth, Play.Enemy.CalcTotalAttackValue());
                            Console.WriteLine("-------------------------------");

                            Console.WriteLine("Select An Option");
                            Console.WriteLine("-------------------------------");

                            Console.WriteLine("1 Attack");
                            Console.WriteLine("2 Next");
                            Console.WriteLine("3 Loot");
                            Console.WriteLine("4 Potion");


                            while (!Int32.TryParse(Console.ReadLine(), out inputNum))
                            {
                                Console.Clear();
                                Console.WriteLine("{0}\t Attack:{1}\tHealth:{2}\n", Play.Player.Name, Play.Player.CalcTotalAttackValue(), Play.Player.CurrentHealth);
                                Console.WriteLine("The {0} has a health of {1} and an attack of {2} Good Luck!", Play.Enemy.Name, Play.Enemy.CurrentHealth, Play.Enemy.CalcTotalAttackValue());
                                Console.WriteLine("-------------------------------");

                                Console.WriteLine("<<TRY AGAIN>>");
                                Console.WriteLine("-------------------------------");

                                Console.WriteLine("1 Attack");
                                Console.WriteLine("2 Next");
                                Console.WriteLine("3 Loot");
                                Console.WriteLine("4 Potion");
                            }

                            switch (inputNum)
                            {
                                case 1:
                                    if (!Play.Enemy.IsDead)
                                    {
                                        last = Play.Enemy.CurrentHealth;
                                        Play.Attack(Play.Player, Play.Enemy);
                                        if (last == Play.Enemy.CurrentHealth)
                                        {
                                            Console.WriteLine("You fool, you missed!");
                                        }
                                        else if (!((last - 8) <= Play.Enemy.CurrentHealth))
                                        {
                                            Console.WriteLine("Woah A Critical Hit!");
                                            Console.WriteLine("Enemy health {0}", Play.Enemy.CurrentHealth);
                                        }
                                        else
                                        {
                                            Console.WriteLine("You landed a hit!");
                                            Console.WriteLine("Enemy health {0}", Play.Enemy.CurrentHealth);
                                        }
                                        correct = false;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Try Looting The Dead Enemy or Go To The Next Battle");
                                        correct = true;
                                    }
                                    break;
                                case 2:
                                    if (Play.Enemy.IsDead)
                                    {
                                        Play.NextBattle();
                                        counter = 0;
                                        try
                                        {
                                            Play.SetEnemy(enemies[Play.Depth]);
                                            Console.Clear();
                                            Console.WriteLine("A {0} Appeared!", Play.Enemy.Name);
                                            Console.ReadLine();
                                            correct = true;
                                        }
                                        catch (Exception)
                                        {
                                            Play.Enemy.SetAttack(ranNum.Next(0, 9));
                                            Play.Enemy.SetDefense(ranNum.Next(0, 9));
                                            Play.SetEnemy(boss[bossNum]);

                                            Console.Clear();
                                            Console.WriteLine("A {0} Appeared!", Play.Enemy.Name);
                                            Console.ReadLine();
                                            bossNum++;
                                            correct = true;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("You Cannot Escape...");
                                    }
                                    break;
                                case 3:
                                    if (Play.Enemy.IsDead && counter == 0)
                                    {
                                        counter++;
                                        Thread.Sleep(milliseconds);
                                        Item nex = box.GenerateNewItem(ranNum.Next(0, 5));

                                        Console.WriteLine("New Item: " + nex.ToString());
                                        time++;

                                        Console.ReadLine();
                                        Console.WriteLine();
                                        Console.WriteLine("Add New Item To Active Inventory? y/n", Play.Player.Name);
                                        input = Console.ReadLine().ToLower();

                                        if (input == "y" || input == "yes")
                                        {
                                            ActiveBagNames[time] = nex.ToString();
                                            ItemBagNames[time] = "empty";
                                        }
                                        else if (input == "no" || input == "n")
                                        {
                                            for (int i = 0; i < ItemBagNames.Length; i++)
                                            {
                                                if (ItemBagNames[i] == "empty")
                                                {
                                                    ItemBagNames[i] = nex.ToString();
                                                    break;
                                                }
                                            }
                                            Console.WriteLine("item was left in stored slot");
                                        }
                                        Console.WriteLine("Active:");
                                        foreach (var item in ActiveBagNames)
                                        {
                                            Console.WriteLine(item);
                                        }
                                        Console.ReadLine();
                                        Console.Clear();
                                        Console.WriteLine("Stored:");
                                        foreach (var item in ItemBagNames)
                                        {
                                            Console.WriteLine(item);
                                        }
                                        correct = true;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Nothing For You To Loot Right Now!");
                                    }
                                    break;
                                case 4:
                                    for (int i = 0; i <= time; i++)
                                    {
                                        if (ActiveBagNames[i] == potions[i].ToString())
                                        {
                                            Console.WriteLine("{0} Used A Potion", Play.Player.Name);
                                            Play.Player.SetDefense(potions[i].HealthBonus);
                                            break;
                                        }
                                        else
                                        {
                                            Console.WriteLine("Sorry You Cannot Use A Potion...");
                                            break;
                                        }
                                    }
                                    if (Play.Enemy.IsDead)
                                    {
                                        correct = true;
                                    }
                                    break;
                            }
                            if (!correct)
                            {
                                Console.WriteLine("\n<<continue>>");
                                Console.ReadLine();
                                last = Play.Player.CurrentHealth;
                                Play.Attack(Play.Enemy, Play.Player);
                                if (last == Play.Player.CurrentHealth)
                                {
                                    Console.WriteLine("Lucky, {0} missed!", Play.Enemy.Name);
                                }
                                else
                                {
                                    Console.WriteLine("Brutal attack from, {0}", Play.Enemy.Name);
                                }
                            }
                            Console.ReadKey();
                        }
                    } while (count == 0);
                }
                catch (Exception a)
                {
                    Console.WriteLine("Sorry,The Game Crashed!");
                    Console.WriteLine("Sending An Error Report To The Developer");
                    Console.ReadKey();
                    Console.WriteLine(a);
                }

                Console.WriteLine("End Game {0}: ", Play.Player.Name);

                Console.WriteLine("Total Inventory");
                for (int i = 0; i < time; i++)
                {
                    Console.WriteLine(ItemBagNames[i]);
                }

                Console.WriteLine("fin.");
                Console.ReadLine();
            }
        }
    }
}
